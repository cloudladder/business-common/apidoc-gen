<?php

return [
    'id' => '主键id',
    'created_at' => '创建时间',
    'updated_at' => '更新时间',
    'deleted_at' => '删除时间',
];
