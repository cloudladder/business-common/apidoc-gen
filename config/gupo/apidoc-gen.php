<?php

declare(strict_types=1);

return [

    // 定义全部接口的公共参数
    'global' => [
    ],

    // 根据前缀定义全部接口的公共参数
    'custom' => [
        'api' => [
            '@apiQuery {string} org_id 用户中心机构id',
            '@apiQuery {string} system_code 用户中心系统编码',
        ],
    ],

    'middleware'       => [
        // 定义api.auth中间件的公共参数
        'auth' => [
            '@apiHeader {string} Authorization 用户token',
        ],
        // 自己有的中间件按照下面的模式声明即可
        //        '中间件名字' => [
        //            '包含此中间件的路由需要携带的公共参数',
        //            '每个参数描述作为此数组的一个元素'
        //        ]
    ],
    // true 接口全扫， false 仅扫描声明了@genName的接口
    'fullScan'         => false,

    // 允许的路由前缀
    'allowed_prefixes' => [
    ],

    // todo 支持自定义注解名字
//    'names' => [
//        'api_name_key' => 'genName',
//        'ignore_key' => 'genIgnore',
//    ],

    // 0 通过代码注释 1 使用table注释+文件配置给字段添加注释
    'rule_mode' => 0,

    // apidoc.php文件输出的物理地址
    'doc' => [
        'output' => '/apidocs/apidoc.php',
    ],

    // request与resource上级命名空间snake化后可作为table属性使用，alias用作把上级命名空间转化成对应table_name
    'table_alias' => [
        'request' => [
        ],
        'resource' => [
        ],
    ],

    // 错误警告开关，所有开关默认开
    'warn_log_switch' => [
        // 未自定义request警告
        'request' => true,
    ]
];

