<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Enums;

/**
 * 官方支持的注释枚举值
 */
enum OfficialAnnotation: string
{
    case API = 'api';
    case API_BODY = 'apiBody';
    case API_DEFINE = 'apiDefine';
    case API_DEPRECATED = 'apiDeprecated';
    case API_DESCRIPTION = 'apiDescription';
    case API_ERROR = 'apiError';
    case API_ERROR_EXAMPLE = 'apiErrorExample';
    case API_EXAMPLE = 'apiExample';
    case API_GROUP = 'apiGroup';
    case API_HEADER = 'apiHeader';
    case API_HEADER_EXAMPLE = 'apiHeaderExample';
    case API_IGNORE = 'apiIgnore';
    case API_NAME = 'apiName';
    case API_PARAM = 'apiParam';
    case API_PARAM_EXAMPLE = 'apiParamExample';
    case API_PERMISSION = 'apiPermission';
    case API_PRIVATE = 'apiPrivate';
    case API_QUERY = 'apiQuery';
    case API_SAMPLE_REQUEST = 'apiSampleRequest';
    case API_SUCCESS = 'apiSuccess';
    case API_SUCCESS_EXAMPLE = 'apiSuccessExample';
    case API_USE = 'apiUse';
    case API_VERSION = 'apiVersion';
}
