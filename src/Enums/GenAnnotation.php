<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Enums;

enum GenAnnotation: string
{

    // 没有request参数
    case GEN_NO_REQUEST = 'genNoRequest';

    // 忽略扫描
    case GEN_IGNORE = 'genIgnore';

    // 标记这是一个分页数据
    case GEN_PAGINATE = 'genPaginate';

    // 定义接口描述
    case GEN_NAME = 'genName';
}
