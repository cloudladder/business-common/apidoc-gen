<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Commands;

use Gupo\ApidocGen\Bootstrap;
use Gupo\ApidocGen\Enums\GenAnnotation;
use Gupo\ApidocGen\Enums\OfficialAnnotation;
use Gupo\ApidocGen\Exports\RouteExport;
use Gupo\ApidocGen\Reflection\ReflectionController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

class RouteExportCommand extends Command
{
    protected $signature = "api:route-export";

    protected $description = "生成接口文档";

    public function handle()
    {
        $routes = Route::getRoutes();

        $data = [];
        foreach ($routes as $route) {
            $uri = $route->uri();
            // 前置是api的才导出
            if (strpos($uri, 'api') !== false && strpos($uri, 'innerapi') === false && strpos($uri, 'openapi') === false) {

                $data[] = $this->getRouteInfo($route);
            } else {
                continue;
            }
        }

        if ($this->export($data)) {
            $this->info('路由导出成功,文件路径: storage/app/public/route_list.xlsx');
        } else {
            $this->info('路由导出失败');
        }
    }

    private function getRouteInfo(\Illuminate\Routing\Route $route)
    {
        // 获取接口名称
        try {
            $controllerName = $route->getControllerClass();
        } catch (\Throwable) {
            return [];
        }

        // action名称
        $actionName = $route->getActionMethod();

        $reflectionController = new ReflectionController($controllerName);

        $reflectionAction = $reflectionController->getAction($actionName);

        $annotations = $this->getActionGenAnnotations($reflectionAction);


        foreach ($annotations as $annotation) {
            if (preg_match('/@genName\s+(.*)/', $annotation, $matches)) {
                $apiName = $matches[1];
            }
        }

        // 获取请求类别
        $methods = $route->methods();

        $filterMethods = array_map('strtolower', array_filter($methods, function ($method) {
            return $method === 'GET' || $method === 'POST';
        }));

        $filterMethods = array_map(function ($method) {
            return '"' . $method . '"';
        }, $filterMethods);


        return [
            'name'        => $apiName ?? $route->uri(),
            'method'      => '[' . implode(',', $filterMethods) . ']',
            'url'         => '/' . $route->uri(),
            'partent_url' => '',
            'auth'        => '是',
            'permission'  => '是',
            'sort'        => '',
        ];
    }

    // 获取方法中定义的apidoc官方注解
    private function getActionGenAnnotations($reflectionAction)
    {
        $docComment = $reflectionAction->getDocComment();

        if ($docComment === false) {
            return [];
        }

        $docComment = str_replace('/*', '', $docComment);
        $docComment = str_replace('*/', '', $docComment);
        $docComment = str_replace('*', '', $docComment);
        $docComment = trim($docComment);

        $officialAnnotation = [];

        if (preg_match_all('/@(\w+)(.*?)(?=(@|$))/s', $docComment, $matches)) {
            foreach ($matches[1] as $i => $key) {

                if (in_array($key, array_column(GenAnnotation::cases(), 'value'))) {
                    $officialAnnotation[] = '@' . $key . $matches[2][$i];
                };
            }
        }
        return $officialAnnotation;
    }

    private function export(array $data): bool
    {
        return Excel::store(new RouteExport($data), 'route_list.xlsx', 'public');
    }
}
