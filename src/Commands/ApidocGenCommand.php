<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Commands;

use Gupo\ApidocGen\Bootstrap;
use Illuminate\Console\Command;

class ApidocGenCommand extends Command
{
    protected $signature = "api:doc-gen";

    protected $description = "生成接口文档";

    public function handle()
    {
        Bootstrap::scan();
        $this->info('文档注释生成完成');
    }
}
