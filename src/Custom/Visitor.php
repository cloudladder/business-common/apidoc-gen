<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Custom;

class Visitor extends \PhpParser\NodeVisitorAbstract
{
    private $result = [];

    public function getResult()
    {
        return $this->result;
    }

    private function iteration(\PhpParser\Node\Expr\Array_|array $array)
    {
        $result = [];
        foreach ($array as $key => $item) {
            if (isset($item->key->value)) {
                $key = $item->key->value;
            }
            if (is_array($item)) {
                if ($key == 'items') {
                    $result = $this->iteration($item);
                } else {
                    $result[$key] = $this->iteration($item);
                }
            }

            if ($item instanceof \PhpParser\Node\ArrayItem) {
                if ($item->value instanceof \PhpParser\Node\Expr\Array_) {
                    if ($key == 'items') {
                        $result[] = $this->iteration($item->value);
                    } else {
                        $result[$key] = $this->iteration($item->value);
                    }
                }
                if ($item->value instanceof \PhpParser\Node\Scalar\String_) {

                    // 如果这个value包含|则切分成数组
                    if (strpos($item->value->value, '|') !== false) {
                        $value = explode('|', $item->value->value);
                    } else {
                        $value = $item->value->value;
                    }
                    if ($key == 'items') {
                        $result[] = $value;
                    } else {
                        $result[$key] = $value;
                    }
                }
            }
        }
        return $result;
    }

    public function leaveNode(\PhpParser\Node $node)
    {
        if ($node instanceof \PhpParser\Node\Stmt\ClassMethod) {
            if ($node->name->toString() === 'scenes') {
                foreach ($node->stmts as $stmt) {
                    if ($stmt instanceof \PhpParser\Node\Stmt\Return_) {
                        if ($stmt->expr instanceof \PhpParser\Node\Expr\Array_) {
                            $this->result = $this->iteration($stmt->expr);
                        }
                    }
                }
            }
        }
    }
}
