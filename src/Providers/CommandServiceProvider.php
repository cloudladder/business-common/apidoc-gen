<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Providers;

use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([__DIR__.'/../../config/gupo/apidoc-gen.php' => config_path('gupo/apidoc-gen.php')], 'config');
        $this->publishes([__DIR__.'/../../config/gupo/wordbook.php' => config_path('gupo/wordbook.php')], 'config');
    }

    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Gupo\ApidocGen\Commands\ApidocGenCommand::class,
                \Gupo\ApidocGen\Commands\RouteExportCommand::class,
            ]);
        }
    }
}
