<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Visitors;

use PhpParser\Node;
use PhpParser\NodeVisitorAbstract;

class ActionReturnsVisitor extends NodeVisitorAbstract
{
    // Action方法名
    private string $actionName;
    // 返回值列表
    private array $returns = [];

    public function __construct($actionName)
    {
        $this->actionName = $actionName;
    }


    public function enterNode(Node $node)
    {
        if ($node instanceof Node\Stmt\ClassMethod && $node->name->toString() === $this->actionName) {
            foreach ($node->stmts as $stmt) {
                if ($stmt instanceof Node\Stmt\Return_) {
                    if ($stmt->expr instanceof Node\Expr\MethodCall) {
                        $methodCall = $stmt->expr;

                        if ($methodCall->var instanceof Node\Expr\Variable && $methodCall->var->name === 'this') {

                            switch ($methodCall->name->toString()) {
                                case 'responseSuccess':
                                    $this->uniquePush(['type' => 'success'], $this->returns);
                                    break;
                                case 'responseError':
                                    $this->uniquePush(['type' => 'error'], $this->returns);
                                    break;
                                case 'responseData':
                                    $resource = '';
                                    foreach ($methodCall->args as $arg) {
                                        if ($arg->value instanceof Node\Expr\ClassConstFetch) {
                                            $resource = $arg->value->class->getParts()[0];
                                        }
                                    }

                                    if (empty($resource)) {
                                        $this->uniquePush(['type' => 'success'], $this->returns);
                                    } else {
                                        $this->uniquePush(['type' => 'data', 'resource' => $resource], $this->returns);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    private function uniquePush($value, &$array)
    {
        if (!in_array($value, $array, true)) {
            $array[] = $value;
        }
    }

    public function getReturns()
    {
        return $this->returns;
    }
}
