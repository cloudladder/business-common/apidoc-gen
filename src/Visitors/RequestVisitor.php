<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Visitors;

use Exception;
use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Scalar\String_;
use PhpParser\NodeVisitorAbstract;

class RequestVisitor extends NodeVisitorAbstract
{
    // 是否存在 rules 方法
    private bool $hasRulesMethod = false;

    // 参数列表
    private array $parameters = [];

    private array $attributes = [];


    // 限制数据类型的验证规则
    private array $typeRules = [
        'array'   => 'Array',
        'boolean' => 'Boolean',
        'numeric' => 'Number',
        'integer' => 'Number',
        'file'    => 'File',
    ];


    public function enterNode(Node $node)
    {
        // 获取 rules 方法
        if ($node instanceof Node\Stmt\ClassMethod && $node->name->toString() === 'rules') {
            $this->hasRulesMethod = true;
            foreach ($node->stmts as $stmt) {
                if ($stmt instanceof Node\Stmt\Return_) {
                    if ($stmt->expr instanceof Expr\Array_) {
                        $this->iterationRules($stmt->expr);
                    } else {
                        throw new Exception('rules 方法返回值不是一个数组');
                    }
                }
            }
        }

        // 收集 attributes 方法
        if ($node instanceof Node\Stmt\ClassMethod && $node->name->toString() === 'attributes') {
            foreach ($node->stmts as $stmt) {
                if ($stmt instanceof Node\Stmt\Return_) {
                    if ($stmt->expr instanceof Expr\Array_) {
                        $this->iterationAttributes($stmt->expr);
                    }
                }
            }
        }
    }

    public function getParameters()
    {
        if ($this->hasRulesMethod === false) {
            throw new Exception('rules 方法不存在');
        }

        foreach ($this->parameters as $key => $item) {
            if ($item['description'] === '') {
                $this->parameters[$key]['description'] = $this->attributes[$key] ?? '';
            }
        }

        return $this->parameters;
    }

    // 迭代rules
    private function iterationRules(Expr\Array_ $expr)
    {
        $parameters = [];
        foreach ($expr->items as $item) {
            $itemRules = [];
            // 收集参数值
            if ($item->key instanceof String_) {
                $parameterKey = $item->key->value;
                // 判断 $parameterKey 是否包含 .* 有的话就替换成 .$
                if (strpos($parameterKey, '.*') !== false) {
                    $parameterKey = str_replace('.*', '.$', $parameterKey);
                }
            }

            // 收集参数的规则列表
            // 参数规则第一种写法：字符串
            if ($item->value instanceof String_) {
                // 这里需要将 字符串类型的规则转换成数组
                $itemRules = explode('|', $item->value->value);
            }

            // 参数规则的第二种写法：数组
            if ($item->value instanceof Expr\Array_) {
                foreach ($item->value->items as $ruleItem) {
                    // 有三种形式 1. 字符串 2. 对象 3. 闭包函数 仅收集处理字符串 其他的忽略
                    if ($ruleItem->value instanceof String_) {
                        $itemRules[] = $ruleItem->value->value;
                    }
                }
            }

            // 获取参数的注释内容 支持多种注释写法
            $comments = [];
            $comments = $this->disposeComment($item->getComments());

            // 参数类型
            $parameterType = 'String';

            // 判断 $ruleArr 中是否有 $typeRules 中的键，有的话就取出对应的值
            foreach ($itemRules as $value) {
                if (in_array($value, array_keys($this->typeRules))) {
                    $parameterType = $this->typeRules[$value];
                    break;
                }
            }

            // 参数是否可选
            $parameterIsOptional = true;

            // 如果参数规则列表中存在 required 或 present 且不存在 nullable 则参数是必填的
            if ((in_array('required', $itemRules) || in_array('present', $itemRules)) && !in_array('nullable', $itemRules)) {
                $parameterIsOptional = false;
            }

            // 读取参数默认值
            if (isset($comments[0])) {
                // 正则匹配默认值和注释
                if (preg_match('/(\{(.*)\})?(\(=(.*)\))?(.*)?/', $comments[0], $matches)) {
                    $parameterType = !empty($matches[2]) ? $matches[2] : $parameterType;
                    $parameterDefaultValue = $matches[4] ?? '';
                    $parameterDescription = $matches[5] ?? '';
                }
            } else {
                $parameterDefaultValue = '';
                $parameterDescription = '';
            }

            // min 和 max
            $parameterMin = '';
            $parameterMax = '';
            foreach ($itemRules as $rule) {
                if (strpos($rule, 'min:') !== false) {
                    $parameterMin = str_replace('min:', '', $rule);
                }

                if (strpos($rule, 'max:') !== false) {
                    $parameterMax = str_replace('max:', '', $rule);
                }
            }

            // $comments 数组的第一个元素是参数的描述
            // 其他元素追加到apidoc

            $parameters[$parameterKey] = [
                'key'           => $parameterKey,
                'type'          => $parameterType,
                'defaultValue'  => $parameterDefaultValue,
                'isOptional'    => $parameterIsOptional,
                'min'           => $parameterMin,
                'max'           => $parameterMax,
                'description'   => $parameterDescription,
                'otherComments' => count($comments) > 1 ? array_slice($comments, 1) : [],
            ];
        }

        $this->parameters = $parameters;
    }

    // 迭代attributes
    private function iterationAttributes(Expr\Array_ $expr)
    {
        $attributes = [];
        foreach ($expr->items as $item) {
            // 参数的key
            if ($item->key instanceof String_) {
                $key = $item->key->value;
            }

            if ($item->value instanceof String_) {
                $attributes[$key] = $item->value->value;
            }
        }

        $this->attributes = $attributes;
    }

    private function disposeComment(array $comments)
    {
        $result = [];
        /** @var \PhpParser\Comment\Doc $comment */
        foreach ($comments as $comment) {
            // 判断是否是多行注释
            $commentText = $comment->getText();
            if (strpos($commentText, "\n") !== false) {
                $commentTextArr = explode("\n", $commentText);
                foreach ($commentTextArr as $commentText) {
                    // 去除注释最前面的 /** * 空格 // # 并去除最后面的 */
                    $commentText = preg_replace('/^\/\*\*|\*\/|\/\/|#|\*/', '', $commentText);
                    $commentText = trim($commentText);
                    !empty($commentText) ? $result[] = $commentText : null;
                }
            } else {
                // 去除注释最前面的 /** * 空格 // # 并去除最后面的 */
                $commentText = preg_replace('/^\/\*\*|\*\/|\/\/|#|\*/', '', $commentText);
                $commentText = trim($commentText);
                !empty($commentText) ? $result[] = $commentText : null;
            }
        }
        return $result;
    }
}
