<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Reflection;

use ReflectionClass;
use RuntimeException;
use ReflectionException;

/**
 * 控制器反射类
 * @package Gupo\ApidocGen\Reflection
 */
class ReflectionController
{
    private ReflectionClass $reflectionClass;

    /**
     * @param string $className 控制器类名
     * @return void
     */
    public function __construct(string $controllerName)
    {
        $this->reflectionClass = new ReflectionClass($controllerName);
    }

    public function getReflectionClass(): ReflectionClass
    {
        return $this->reflectionClass;
    }

    /**
     * 获取控制器中的Action
     * @param string $actionName
     * @return ReflectionAction
     * @throws RuntimeException
     * @throws ReflectionException
     */
    public function getAction(string $actionName): ReflectionAction
    {
        $fileName = $this->reflectionClass->getFileName();
        if ($fileName === false) {
            throw new \RuntimeException('获取类文件路径失败');
        }

        return new ReflectionAction($this->reflectionClass->getMethod($actionName));
    }

    public function getDocComment(): string|false
    {
        return $this->reflectionClass->getDocComment();
    }
}
