<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Reflection;

use Gupo\ApidocGen\Enums\GenAnnotation;
use Gupo\ApidocGen\Enums\OfficialAnnotation;
use ReflectionMethod;
use ReflectionNamedType;

/**
 * 反射Action
 * @package Gupo\ApidocGen\Reflection
 */
class ReflectionAction
{

    /**
     * @var ReflectionMethod
     */
    private ReflectionMethod $reflectionMethod;

    public function __construct(ReflectionMethod $reflectionMethod)
    {
        $this->reflectionMethod = $reflectionMethod;
    }

    public function getReflectionMethod(): ReflectionMethod
    {
        return $this->reflectionMethod;
    }


    public function getDocComment(): string|false
    {
        return $this->reflectionMethod->getDocComment();
    }

    public function getParameters(): array
    {
        return $this->reflectionMethod->getParameters();
    }

    /**
     * 获取自定义Request参数
     * @param mixed $requestName
     * @return bool
     */
    public function getCustomRequestParameter(&$requestName): bool
    {
        $parameters = $this->getParameters();

        foreach ($parameters as $parameter) {

            $parameterType = $parameter->getType();

            if ($parameterType instanceof ReflectionNamedType) {

                // 获取参数类型名
                $parameterTypeName = $parameterType->getName();

                // 判断是否有注入 Request 类型，如果没有给出警告。
                if (strpos($parameterTypeName, 'Request') !== false && trim($parameterTypeName, '') !== 'Illuminate\Http\Request') {
                    $requestName = $parameterTypeName;
                    return true;
                }
            }
        }

        return false;
    }

    public function gatherApiDocAnnotations()
    {
        $apiDocAnnotation = [];
        $docComment = $this->getDocComment();

        if ($docComment === false) {
            return [];
        }

        $docComment = str_replace('/*', '', $docComment);
        $docComment = str_replace('*/', '', $docComment);
        $docComment = str_replace('*', '', $docComment);
        $docComment = trim($docComment);


        if (preg_match_all('/@(\w+)(.*?)(?=(@|$))/s', $docComment, $matches)) {
            foreach ($matches[1] as $i => $key) {

                if (in_array($key, array_column(OfficialAnnotation::cases(), 'value'))) {
                    $officialAnnotation[] = '@' . $key . $matches[2][$i];
                };

                if (in_array($key, array_column(GenAnnotation::cases(), 'value'))) {
                    $genAnnotation[] = '@' . $key . $matches[2][$i];
                };
            }

            $this->officialAnnotation = $officialAnnotation;
            $this->genAnnotation = $genAnnotation;
        }
    }

    /**
     * 判断是否存在Request
     * @return bool
     */
    public function hasRequest(): bool
    {
        $docComment = $this->getDocComment();

        if ($docComment === false) {
            return false;
        }

        $docComment = str_replace('/*', '', $docComment);
        $docComment = str_replace('*/', '', $docComment);
        $docComment = str_replace('*', '', $docComment);
        $docComment = trim($docComment);

        if (preg_match_all('/@(\w+)(.*?)(?=(@|$))/s', $docComment, $matches)) {

            foreach ($matches[1] as $i => $key) {
                if ($key == GenAnnotation::GEN_NO_REQUEST->value) {
                    return false;
                };
            }
        }

        return true;
    }
}
