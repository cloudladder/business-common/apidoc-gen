<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Reflection;

use Composer\InstalledVersions;
use Error;
use Gupo\ApidocGen\Custom\Visitor;
use Gupo\ApidocGen\File;
use Gupo\ApidocGen\Visitors\RequestVisitor;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Throwable;

/**
 * 反射 Request 类
 * @package Gupo\ApidocGen\Reflection
 */
class ReflectionRequest
{

    private ReflectionClass $reflectionClass;

    private string $actionName;

    // 限制数据类型的验证规则
    private array $typeRules = [
        'array'   => 'Array',
        'boolean' => 'Boolean',
        'numeric' => 'Number',
        'integer' => 'Number',
        'file'    => 'File',
    ];

    private array $parameters = [];


    public function __construct(string $requestName, string $actionName)
    {
        $this->reflectionClass = new ReflectionClass($requestName);
        $this->actionName = $actionName;
    }

    public function format(): array
    {
        $scenesMethod = $this->getMethod('scenes');

        $rulesMethod = $this->getMethod('rules');

        $attributesMethod = $this->getMethod('attributes');

        $sceneMode = !empty($scenesMethod);


        if (empty($rulesMethod)) {
            error_log('警告: ' . $this->reflectionClass->getName() . ' 没有声明rules方法。');
            return $this->parameters;
        }

        if (empty($attributesMethod)) {
            error_log('警告: ' . $this->reflectionClass->getName() . ' 没有声明attributes方法。');
        }

        $rulesMethodContent = File::getContent($this->reflectionClass->getFileName(), $rulesMethod->getStartLine(), $rulesMethod->getEndLine());

        if (!empty($attributesMethod)) {
            $attributesMethodContent = File::getContent($this->reflectionClass->getFileName(), $attributesMethod->getStartLine(), $attributesMethod->getEndLine());
        }

        if (preg_match_all("/[\'\"](.*?)[\'\"]\s*=>\s*\[(.*?)\][,\s]*(\/\/\s*(\(\=(.*?)\))?(\s*(.*))?)?/", $rulesMethodContent, $ruleMatches)) {

            foreach ($ruleMatches[1] as $key => $parameterName) {
                $rules[$parameterName] = [
                    'rule'         => $ruleMatches[2][$key],
                    'defaultValue' => $ruleMatches[5][$key] ?? '',
                    'description'  => $ruleMatches[7][$key] ?? '',
                ];
            }
        }

        if (empty($rules)) {
            error_log('警告: ' . $this->reflectionClass->getName() . ' rules 方法返回值无法解析');
            return $this->parameters;
        }

        // 构建attributes的返回值数组
        if (preg_match_all("/[\'\"]([^']*)[\'\"] => [\'\"]([^']*)[\'\"][,\\n]/", $attributesMethodContent ?? '', $attributeMatches)) {
            $attributes = array_combine($attributeMatches[1], $attributeMatches[2]);
        }

        if ($sceneMode) {
            // 先不支持场景模式
            return $this->parameters;
            // $this->sceneModeFormat($rules, $attributes ?? []);
        } else {
            $this->normalFormat($rules, $attributes ?? []);
        }

        return $this->parameters;
    }

    // 场景模式下格式化
    private function sceneModeFormat($rules, $attributes)
    {
        $parserVersion = InstalledVersions::getVersion('nikic/php-parser');
        if (version_compare($parserVersion, '5.0.0', '>=')) {
            $parser = (new ParserFactory)->createForHostVersion();
        } else {
            $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        }
        try {
            $ast = $parser->parse(file_get_contents($this->reflectionClass->getFileName()));
            $traverser = new NodeTraverser();

            $visitor = new Visitor();

            $traverser->addVisitor($visitor);
            $traverser->traverse($ast);
            $scenesResult = $visitor->getResult();

            if (in_array($this->actionName, array_keys($scenesResult))) {
                $scenesRules = $scenesResult[$this->actionName];
                foreach ($scenesRules as $field => $scenesRule) {
                    $paramType = 'String';

                    $realRules = [];
                    if (empty($scenesRule)) {
                        continue;
                    }
                    // 如果是数字索引则去rules中取验证规则
                    if (is_int($field)) {
                        $realRules = array_merge($realRules, $this->ruleStrToRuleArr($rules[$scenesRule]['rule'] ?? ''));
                        $field = $scenesRule;
                    } else {
                        if (!empty($item)) {
                            $realRules = array_merge($realRules, $item);
                        }
                    }

                    // 判断 $realRules 中是否有 $typeRules 中的键，有的话就取出对应的值
                    foreach ($realRules as $value) {
                        if (in_array($value, array_keys($this->typeRules))) {
                            $paramType = $this->typeRules[$value];
                            break;
                        }
                    }
                    // 如果参数是可选的则定义可选类型
                    $isOptional = in_array('nullable', $realRules) ? true : false;

                    $rule = $rules[$scenesRule] ?? [];
                }
            }
        } catch (Error $e) {
            error_log('Parse Error: ' . $e->getMessage());
        }
    }

    /**
     * 标准模式下格式化
     * @return string
     */
    private function normalFormat()
    {
        $parserVersion = InstalledVersions::getVersion('nikic/php-parser');
        if (version_compare($parserVersion, '5.0.0', '>=')) {
            $parser = (new ParserFactory)->createForHostVersion();
        } else {
            $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        }

        $ast = $parser->parse(file_get_contents($this->reflectionClass->getFileName()));
        $traverser = new NodeTraverser();

        $visitor = new RequestVisitor();
        $traverser->addVisitor($visitor);

        try {
            $traverser->traverse($ast);
            $this->parameters = $visitor->getParameters();
        } catch (Throwable $e) {
            error_log($this->reflectionClass->getName() . ' ' . $e->getMessage());
        }
    }

    /**
     * 将验证规则字符串转换为数组
     * @return array
     */
    public function ruleStrToRuleArr(string $ruleStr)
    {
        // 是一个由逗号或者竖线分隔的字符串
        $delimiter = strpos($ruleStr, '|') !== false ? '|' : ',';
        $ruleArr = explode($delimiter, str_replace(' ', '', $ruleStr));

        // 清理掉每个元素的'和"
        return array_map(function ($item) {
            return str_replace(['"', "'"], '', $item);
        }, $ruleArr);
    }

    /**
     * 获取Request类的注释
     * @param string $methodName
     * @return ?ReflectionMethod
     */
    private function getMethod(string $methodName): ?ReflectionMethod
    {

        try {
            $method = $this->reflectionClass->getMethod($methodName);

            if ($method->getDeclaringClass()->getName() == $this->reflectionClass->getName()) {
                return $method;
            }
        } catch (ReflectionException) {
            return null;
        }
        return null;
    }
}
