<?php

declare(strict_types=1);

namespace Gupo\ApidocGen\Reflection;

use App\Http\Resources\Apply\ShowResource;
use Composer\InstalledVersions;
use Gupo\ApidocGen\Enums\GenAnnotation;
use Gupo\ApidocGen\File;
use Gupo\ApidocGen\Visitors\ActionReturnsVisitor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\DocBlockFactory;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Throwable;

/**
 * 反射 Resource 类
 * @package Gupo\ApidocGen\Reflection
 */
class ReflectionResource
{

    private ReflectionClass $refController;

    private ReflectionMethod $refAction;

    public function __construct(ReflectionClass $refController, ReflectionMethod $refAction)
    {
        $this->refController = $refController;
        $this->refAction = $refAction;
    }

    public function format()
    {
        $returns = $this->getReturns();

        $result = '';
        foreach ($returns as $return) {
            switch ($return['type']) {
                case 'success':
                    $result .= ' * @apiSuccess {Number} code 200' . PHP_EOL
                        . ' * @apiSuccess {String} message 操作成功' . PHP_EOL
                        . ' * @apiSuccess {Array} data []' . PHP_EOL
                        . ' * @apiSuccessExample {Json} Success-Response:' . PHP_EOL
                        . ' * { "code": 200, "message": "操作成功", "data": [] }' . PHP_EOL;
                    break;
                case 'error':
                    $result .= ' * @apiError {Number} code 40010' . PHP_EOL
                        . ' * @apiError {String} message 操作失败' . PHP_EOL
                        . ' * @apiError {Array} data []' . PHP_EOL
                        . ' * @apiErrorExample {Json} Error-Response:' . PHP_EOL
                        . ' * { "code": 40010, "message": "操作失败", "data": [] }' . PHP_EOL;
                    break;
                case 'data':

                    $resourceName = ($this->getClassFullName($this->refController->getFileName(), $return['resource']));

                    $exampleArr = [
                        'code'    => 200,
                        'message' => '操作成功',
                        'data'    => [],
                    ];
                    $result .= ' * @apiSuccess {Number} code 200' . PHP_EOL
                        . ' * @apiSuccess {String} message 成功消息' . PHP_EOL
                        . ' * @apiSuccess {Array|Object} data' . PHP_EOL;

                    $data = $this->parseResource($resourceName, 'data.', $result);
                    if (config('gupo.apidoc-gen.rule_mode') !== 1) {
                        $exampleArr['data'] = $data;
                        // 构建出 @SuccessExample
                        $json = json_encode($exampleArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

                        $lines = explode("\n", $json);
                        $lines = array_map(function ($line) {
                            return " * " . $line;
                        }, $lines);

                        $result .= ' * @apiSuccessExample {json} Success-Response:' . PHP_EOL;
                        $result .= implode("\n", $lines) . PHP_EOL;
                    }

                    break;
            }
        }


        return $result;
    }

    /**
     * 递归构建资源返回
     * @param string $resourceName
     * @param string $result
     * @param array $exampleArr
     * @return mixed
     */
    public function parseResource(string $resourceName, string $str, string &$result)
    {
        $data = [];
        try {
            $reflectionResource = new ReflectionClass($resourceName);
            $isPaginate = $this->isPaginate();

            $toArrayMethod = $reflectionResource->getMethod('toArray');
        } catch (ReflectionException $e) {
            dump($e->getMessage(), $e->getTraceAsString());
            return $data;
        }

        $toArrayMethodContent = File::getContent($reflectionResource->getFileName(), $toArrayMethod->getStartLine(), $toArrayMethod->getEndLine());
        // 正则匹配拿到 toArray 方法的 return 内容
        if (preg_match('/return([^;]*);/s', $toArrayMethodContent, $matches)) {

            $returnStr = $matches[1];
            if (config('gupo.apidoc-gen.rule_mode') === 1) {
                preg_match_all("/(?<=\s'\b|\")(\w+)(?='\s*=>|\")/", $returnStr, $matches);
                $keys = $matches[0]?? [];

                $table_name = data_get($resourceName::make([]), 'table');
                if (empty($table_name)) {
                    $table_name = Str::snake(basename(dirname(str_replace('\\', '/', $resourceName))));
                    $table_name = config('gupo.apidoc-gen.table_alias.resource')[$table_name] ?? $table_name;
                }
                $columns = DB::connection(config('database.default'))
                    ->table('information_schema.COLUMNS')
                    ->where('TABLE_NAME', $table_name)
                    ->where('COLUMN_COMMENT', '!=', '')
                    ->select(['COLUMN_COMMENT', 'COLUMN_NAME', 'COLUMN_TYPE'])->get();
                $_c =
                    collect(config('gupo.wordbook'))->map(fn($v, $k) => json_decode(json_encode([
                        'COLUMN_NAME' => $k,
                        'COLUMN_COMMENT' => $v,
                        'COLUMN_TYPE' => 'String'
                    ])));
                $columns = $columns->merge($_c);
                if ($columns->count() > 0) {
                    $prefix = match (true) {
                        $isPaginate => 'data.list.0.',
                        default => 'data.'
                    };
                    $result .= collect($keys)->reduce(function ($initial, $item) use ($prefix, $columns) {
                        $column = $columns->where('COLUMN_NAME', $item)->first();
                        $_type = $column?->COLUMN_TYPE;
                        $type = match (true) {
                            \Str::contains($_type, 'int') => 'Number',
                            default => 'String'
                        };
                        return $initial ." * @apiSuccess {{$type}} {$prefix}{$item} {$column?->COLUMN_COMMENT}" . PHP_EOL;
                    });
                }

            } elseif (preg_match_all("/(.*)?'(.*)'\s*=>\s*(.*),\s*\/\/\s*\((.*)\)\s*(.*)/", $returnStr, $matches)) {

                /** @var array $lines */
                $lines = $matches[0];

                /** @var array $lines */
                $start = $matches[1];

                /** @var array $key */
                $key = $matches[2];
                /** @var array $value */
                $value = $matches[3];
                /** @var array $type */
                $type = $matches[4];

                // 将 type 转换为首字母大写
                $type = array_map(function ($item) {
                    return ucfirst($item);
                }, $type);

                $comment = $matches[5];

                $typeDefault = array_map(function ($item) {
                    $defaults = [
                        "string"  => '',
                        'number'  => 0,
                        'boolean' => true,
                        'array'   => [],
                        'object'  => '{}',
                    ];
                    return $defaults[strtolower($item)] ?? '';
                }, $type);

                if ($isPaginate) {
                    $data = [
                        'paginate' => [
                            'total'        => 10,
                            'current_page' => 10,
                            'page_size'    => 15,
                        ],
                        'list'     => [],
                    ];
                }

                if ($isPaginate) {
                    $str = $str . 'list.0.';
                }

                for ($i = 0; $i < count($lines); $i++) {

                    if (strpos($start[$i], '//') !== false) {
                        continue;
                    }
                    $result .= ' * @apiSuccess {' . $type[$i] . '} ' . $str . $key[$i] . ' ' . $comment[$i] . PHP_EOL;
                    if ($isPaginate) {

                        // 判断$value是否包含Resource
                        if (preg_match_all('/((\w+Resource)::make|(\w+Resource)::collection|new (\w+Resource))\(.*?\)/', $value[$i], $matches)) {
                            $resourceName = $this->getClassFullName($reflectionResource->getFileName(), $matches[3][0]);
                            $data['list'][0][$key[$i]] = $this->parseResource($resourceName, $str . $key[$i] . '.', $result);
                        } else {
                            $data['list'][0][$key[$i]] = $typeDefault[$i];
                        }
                    } else {
                        // 匹配出下级是否包含Resource
                        if (preg_match_all('/((.*?Resource)::make|(.*?Resource)::collection|new (.*?Resource))\(.*?\)/', $value[$i], $matches)) {

                            $resourceName = $this->getClassFullName(
                                $reflectionResource->getFileName(),
                                !empty($matches[2][0]) ? $matches[2][0] : (!empty($matches[3][0]) ? $matches[3][0] : $matches[4][0])
                            );
                            $data[$key[$i]] = $this->parseResource($resourceName, $str . $key[$i] . '.', $result);
                        } else {
                            $data[$key[$i]] = $typeDefault[$i];
                        }
                    }
                }
            } else {
                error_log('警告: ' . $resourceName . ' 类 toArray 方法的返回值未注释数据类型。');
            };
            return $data;
        }
    }

    private function getReturns(): array
    {
        $returns = [];
        $parserVersion = InstalledVersions::getVersion('nikic/php-parser');
        if (version_compare($parserVersion, '5.0.0', '>=')) {
            $parser = (new ParserFactory)->createForHostVersion();
        } else {
            $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        }

        $ast = $parser->parse(file_get_contents($this->refController->getFileName()));

        $traverser = new NodeTraverser();

        $visitor = new ActionReturnsVisitor($this->refAction->getName());
        $traverser->addVisitor($visitor);

        try {
            $traverser->traverse($ast);
            $returns = $visitor->getReturns();
            return $returns;
        } catch (Throwable $e) {
            error_log($this->refController->getName() . ' ' . $e->getMessage());
        }
    }

    // 获取文件中调用的类的全名
    private function getClassFullName(string $filePath, $class): string
    {
        $file = file($filePath);

        // 引入类的文件列表

        $useList = preg_grep('/^use/', $file);

        // 当前命名空间;
        $namespace = preg_grep('/^namespace/', $file);

        // 找到匹配的引入类
        $fullClass = array_filter($useList, function ($item) use ($class) {
            return strpos($item, $class) !== false;
        });

        $fullClass = end($fullClass);

        if ($fullClass === false) {
            // 在 use 列表里面没找到，可能是当前命名空间的类
            $namespace = end($namespace);

            $namespace = preg_replace('/namespace/', '', $namespace);
            $namespace = preg_replace('/;/', '', $namespace);
            $namespace = trim($namespace);
            $fullClass = $namespace . '\\' . $class;
        } else {
            // 正则匹配删除 ' as '，删除它及其后面的所有字符
            $fullClass = preg_replace('/ as .*/', '', $fullClass);

            // 删除 'use ' 和末尾的分号
            $fullClass = str_replace('use ', '', $fullClass);
            // 删除分号
            $fullClass = str_replace(';', '', $fullClass);
            // 删除空格
            $fullClass = trim($fullClass);
        }

        return $fullClass;
    }

    /**
     * 是否分页返回
     * @return bool
     */
    private function isPaginate(): bool
    {
        $comment = $this->refAction->getDocComment();

        if (!$comment) return false;

        $pattern = '/@' . GenAnnotation::GEN_PAGINATE->value . '/';

        if (preg_match($pattern, $comment)) {
            return true;
        } else {
            return false;
        }
    }
}
