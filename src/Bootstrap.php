<?php

namespace Gupo\ApidocGen;

use Illuminate\Support\Facades\Route;
use Gupo\ApidocGen\Reflection\ReflectionController;
use Throwable;

class Bootstrap
{
    public static function scan()
    {
        $routes = Route::getRoutes();
        $apidoc = '<?php' . PHP_EOL . PHP_EOL . PHP_EOL;

        /** @var \Illuminate\Routing\Route $route */
        foreach ($routes as $route) {
            // 判断 $route->uri() 是否是 api 或者 innerapi 或者 openapi 开头
            if (strpos($route->uri(), 'api') !== 0 && strpos($route->uri(), 'innerapi') !== 0 && strpos($route->uri(), 'openapi') !== 0) {
                continue;
            }
            // 路由的uri
            $uri = $route->uri();

            if (self::isIgnoreRoute($route)) {
                continue;
            }

            // 路由支持的请求方法列表
            $api = '';
            $methods = $route->methods();
            foreach ($methods as $key => $method) {

                if ($method === 'HEAD') {
                    unset($methods[$key]);
                } else {
                    $methods[$key] = strtolower($method);
                }
            }

            // 将 [get,post,put] 转换成 'get|post|put'
            $api .= ' * @api {' . implode('|', $methods) . '} ' . $uri;

            // 路由的控制器
            try {

                try {
                    // 这里如果报错，说明路由没有绑定控制器，这种情况跳过
                    $controllerClass = $route->getControllerClass();
                } catch (Throwable $e) {
                    continue;
                }

                $controllerName = $controllerClass;
                // 判断是否是写在Http/Controllers目录下的控制器
                if (strpos($controllerName, 'App\Http\Controllers') !== 0) {
                    continue;
                }
            } catch (Throwable $e) {

                error_log($e->getMessage() . PHP_EOL . $e->getTraceAsString());
                continue;
            }

            // action名称
            $actionName = $route->getActionMethod();

            $reflectionController = new ReflectionController($controllerName);

            $reflectionAction = $reflectionController->getAction($actionName);

            $format = new Format($reflectionController, $reflectionAction);

            $apidoc .= $format->output($route);
        }

        File::writeApiDoc($apidoc);
    }

    // 是否忽略路由
    private static function isIgnoreRoute($route): bool
    {
        // 允许的路由前缀
        $allowedPrefixes = config('gupo.apidoc-gen.allowed_prefixes');

        if (empty($allowedPrefixes)) {
            return false;
        }

        $ignore = false;

        foreach ($allowedPrefixes as $prefix) {
            if (! str_starts_with($route->uri, $prefix)) {
                $ignore = true;
                break;
            }
        }

        return $ignore;
    }
}
