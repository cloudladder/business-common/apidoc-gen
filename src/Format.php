<?php

declare(strict_types=1);

namespace Gupo\ApidocGen;

use Gupo\ApidocGen\Enums\GenAnnotation;
use Gupo\ApidocGen\Enums\OfficialAnnotation;
use Gupo\ApidocGen\Reflection\ReflectionAction;
use Gupo\ApidocGen\Reflection\ReflectionController;
use Gupo\ApidocGen\Reflection\ReflectionRequest;
use Gupo\ApidocGen\Reflection\ReflectionResource;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Format
{
    private ReflectionController $reflectionController;

    private ReflectionAction $reflectionAction;

    private Route $route;

    // 自定义注解收集
    private array $officialAnnotation = [];

    // gen工具注解收集
    private array $genAnnotation = [];

    // true - 全扫模式，false - 仅扫描定义了@genName的Action
    private bool $fullScan = true;

    public function __construct(ReflectionController $reflectionController, ReflectionAction $reflectionAction)
    {
        $this->reflectionController = $reflectionController;
        $this->reflectionAction = $reflectionAction;
        $this->fullScan = config('gupo.apidoc-gen.full_scan', true);
    }

    /**
     * 格式化后输出
     *
     * @return string
     */
    public function output(Route $route): string
    {
        $controllerComment = $this->reflectionController->getDocComment();
        $controllerComment = $controllerComment ? $controllerComment : '';
        $this->controllerComment = $controllerComment;

        // 初始化一些
        $this->initActionAnnotations();

        $this->route = $route;

        // 如果不是全扫描模式，且没有定义@genName，就不输出
        if (! $this->fullScan && ! $this->isDeclaredGenName()) {
            return '';
        }

        // 如果是忽略扫描的Action，就不输出
        if ($this->fullScan && $this->isDeclaredGenIgnore()) {
            return '';
        }

        // 输出单个接口的文档
        $apidoc = '/**' . PHP_EOL;

        $apidoc .= $this->formatApiUri();
        $apidoc .= $this->formatApiVersion();
        $apidoc .= $this->formatApiGroup();
        $apidoc .= $this->formatConfigOutput($route);
        foreach ($this->officialAnnotation as $annotation) {
            // 如果有@apiGroup，就不再重复输出
            if (preg_match('/@apiGroup/', $annotation)) {
                continue;
            }
            $apidoc .= ' * ' . $annotation . PHP_EOL;
        }


        // 如果标记了没有Request参数，就不要去解析Request参数了
        if ($this->reflectionAction->hasRequest()) {
            $apidoc .= $this->formatRequest($route);
        }
        $apidoc .= $this->formatResponse();

        $apidoc .= ' */' . PHP_EOL . PHP_EOL;
        return $apidoc;
    }

    /**
     * 格式化 url 定义部分
     *
     * @return string
     */
    private function formatApiUri(): string
    {
        $genName = $this->getGenName();

        $methods = $this->route->methods();
        $api = '';

        foreach ($methods as $key => $method) {
            if ($method === 'HEAD') {
                unset($methods[$key]);
            } else {
                $methods[$key] = strtolower($method);
            }
        }

        // 将 [GET,POST,PUT] 转换成 'get|post|put'
        $api .= ' * @api {' . implode('|', $methods) . '} ' . $this->route->uri();

        return $api . ' ' . $genName . PHP_EOL;

    }

    /**
     * 格式化接口版本
     *
     * @return string
     */
    private function formatApiVersion(): string
    {
        return ' * @apiVersion 1.0.0' . PHP_EOL;
    }

    /**
     * 格式化接口组
     *
     * @return string
     */
    private function formatApiGroup(): string
    {
        $controllerComment = $this->controllerComment;

        $apiGroup = '';

        // 如果Action有定义apiGroup，就使用Action的apiGroup
        foreach ($this->officialAnnotation as $annotation) {
            if (preg_match('/@apiGroup\s+(.*)\n/', $annotation, $matches)) {
                $apiGroup = $matches[1];
            }
        }

        // 如果Action中未定义apiGroup，就使用Controller中定义apiGroup
        if (preg_match('/@apiGroup\s+(.*)\n/', $controllerComment, $matches) && empty($apiGroup)) {
            $apiGroup = $matches[1] ?? null;
        }

        // 如果都未定义则使用控制器全名作为默认的apiGroup
        if (empty($apiGroup)) {
            error_log('警告: ' .
                      $this->reflectionController->getReflectionClass()->getName() .
                      ' 的 ' .
                      $this->reflectionAction->getReflectionMethod()->getName() .
                      ' 方法没有定义@apiGroup。');
            $apiGroup = $this->reflectionController->getReflectionClass()->getName();
        }

        return ' * @apiGroup ' . $apiGroup . PHP_EOL;
    }

    private function getGenName()
    {
        $genName = '';
        foreach ($this->genAnnotation as $annotation) {
            if (preg_match('/@genName\s+(.*)/', $annotation, $matches)) {
                $genName = $matches[1];
            }
        }

        return $genName;
    }

    /**
     * 格式化请求参数
     *
     * @return string
     */
    private function formatRequest(Route $route): string
    {
        if ($this->reflectionAction->getCustomRequestParameter($requestName)) {

            $apiParam = '@apiQuery';
            $method = $route->methods();

            $method = in_array('POST', $method) ? '@apiBody' : '@apiQuery';

            if (config('gupo.apidoc-gen.rule_mode') === 1) {
                $result = '';
                $parameters = (new $requestName())->rules();
                $attributes = (new $requestName())->attributes();
                $table_name = data_get(new $requestName(), 'table');
                if (empty($table_name)) {
                    $table_name = Str::snake(basename(dirname(str_replace('\\', '/', $requestName))));
                    $table_name = config('gupo.apidoc-gen.table_alias.request')[$table_name] ?? $table_name;
                }
                $columns = DB::connection(config('database.default'))
                    ->table('information_schema.COLUMNS')
                    ->where('TABLE_NAME', $table_name)
                    ->where('COLUMN_COMMENT', '!=', '')
                    ->pluck('COLUMN_COMMENT', 'COLUMN_NAME')->toArray();
                $columns = array_merge($columns, config('gupo.wordbook'));
                $attributes = array_merge($columns, $attributes);
                foreach ($parameters as $key => $rules) {
                    $p = match (in_array('required', $rules)) {
                        true => "$key",
                        default => "[$key]",
                    };
                    $type = match (true) {
                        in_array('int', $rules) => "{int}",
                        in_array('array', $rules) => "{array}",
                        default => "{string}"
                    };
                    $attribute = $attributes[$key] ?? '';
                    $result .= " * {$method} {$type} {$p} {$attribute} ". PHP_EOL;
                }

                return $result .= ' * ' . PHP_EOL;
            }

            $parameters =
                (new ReflectionRequest($requestName,
                    $this->reflectionAction->getReflectionMethod()->getName()))->format();

            if (empty($parameters)) {
                return '';
            }

            $result = '';
            foreach ($parameters as $parameter) {

                $size = '';
                if (! empty($parameter['min']) || ! empty($parameter['max'])) {
                    if ($parameter['type'] === 'string') {
                        $size = '{' . $parameter['min'] . '..' . $parameter['max'] . '}';
                    } else {
                        $size = '{' . $parameter['min'] . '-' . $parameter['max'] . '}';
                    }
                }

                $result .= ' * ' .
                           $method .
                           ' {' .
                           $parameter['type'] .
                           $size .
                           '} ' .
                           ($parameter['isOptional'] ? '[' : '') .
                           $parameter['key'] .
                           (empty($parameter['defaultValue']) ? '' : '=' . $parameter['defaultValue']) .
                           ($parameter['isOptional'] ? ']' : '') .
                           ' ' .
                           $parameter['description'] .
                           PHP_EOL;
                if (! empty($parameter['otherComments'])) {
                    foreach ($parameter['otherComments'] as $comment) {
                        $result .= ' * ' . $comment . PHP_EOL;
                    }
                }
            }
            return $result .= ' * ' . PHP_EOL;
        };

        if (config('gupo.apidoc-gen.warn_log_switch.request', true)) {
            error_log('警告: ' .
                      $this->reflectionController->getReflectionClass()->getName() .
                      ' 的 ' .
                      $this->reflectionAction->getReflectionMethod()->getName() .
                      ' 方法没有声明自定义 Request 参数。');
        }
        return '';
    }

    /**
     * 格式化响应参数
     *
     * @return string
     */
    private function formatResponse(): string
    {
        return (new ReflectionResource($this->reflectionController->getReflectionClass(),
            $this->reflectionAction->getReflectionMethod()))->format();
    }

    /**
     * 格式化配置注入
     *
     * @return string
     */
    private function formatConfigOutput(Route $route): string
    {
        $result = '';
        // 追加全局的文档配置
        $globalList = config('gupo.apidoc-gen.custom', []);

        foreach ($globalList as $prefix => $params) {
            $params = array_unique(array_merge(config('gupo.apidoc-gen.global', []), $params));
            foreach ($params as $value) {
                if (str_starts_with($route->uri(), $prefix)) {
                    $result .= ' * ' . $value . PHP_EOL;
                }
            }
        }


        // 追加对特定中间件的处理

        $apidocGenConfig = config('gupo.apidoc-gen.middleware', []);
        $configKeys = array_keys($apidocGenConfig);

        foreach ($this->route->gatherMiddleware() as $middleware) {
            if (in_array($middleware, $configKeys)) {
                foreach ($apidocGenConfig[$middleware] as $key => $value) {
                    $result .= ' * ' . $value . PHP_EOL;
                }
            }
        }
        return $result;
    }

    /**
     * 判断是否有定义@genName
     *
     * @return bool
     */
    public function iSDeclaredGenName(): bool
    {
        $docComment = $this->reflectionAction->getDocComment();
        $docComment = $docComment ? $docComment : '';

        $name_key = config('gupo.apidoc-gen.names.api_name_key', 'genName');

        // 匹配是否有 @genName
        return preg_match("/@{$name_key}/", $docComment) === 1;
    }

    /**
     * 判断是否有定义@genIgnore
     *
     * @return bool
     */
    public function isDeclaredGenIgnore(): bool
    {
        $docComment = $this->reflectionAction->getDocComment();
        $docComment = $docComment ? $docComment : '';

        $ignore_key = config('gupo.apidoc-gen.names.ignore_key', 'genIgnore');

        // 匹配是否有 @genIgnore
        return preg_match("/@{$ignore_key}/", $docComment) === 1;
    }


    public function initActionAnnotations()
    {
        $docComment = $this->reflectionAction->getDocComment();

        if ($docComment === false) {
            return [];
        }

        $docComment = str_replace('/*', '', $docComment);
        $docComment = str_replace('*/', '', $docComment);
        $docComment = str_replace('*', '', $docComment);
        $docComment = trim($docComment);


        if (preg_match_all('/@(\w+)(.*?)(?=(@|$))/s', $docComment, $matches)) {
            foreach ($matches[1] as $i => $key) {

                if (in_array($key, array_column(OfficialAnnotation::cases(), 'value'))) {
                    $officialAnnotation[] = '@' . $key . $matches[2][$i];
                };

                if (in_array($key, array_column(GenAnnotation::cases(), 'value'))) {
                    $genAnnotation[] = '@' . $key . $matches[2][$i];
                };
            }

            $this->officialAnnotation = $officialAnnotation ?? [];
            $this->genAnnotation = $genAnnotation ?? [];
        }
    }
}
