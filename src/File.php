<?php

declare(strict_types=1);

namespace Gupo\ApidocGen;

class File
{
    public static function getContent(string $fileName, int $startLine, int $endLine): string
    {

        $file = file($fileName);

        $content = '';

        for ($i = $startLine; $i <= $endLine; $i++) {
            if (isset($file[$i])) $content .= $file[$i];
        }

        return $content;

        return file_get_contents($fileName);
    }

    /**
     * 写入接口文件
     * @param string $filePath
     * @param string $content
     * @return void
     */
    public static function writeApiDoc(string $content)
    {

        $filePath = getcwd() . '/' . trim(config('gupo.apidoc-gen.doc.output', '/apidocs/apidoc.php'), '/');

        $dir = pathinfo($filePath, PATHINFO_DIRNAME);

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        file_put_contents($filePath, $content);
    }
}
