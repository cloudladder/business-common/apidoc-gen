<?php

namespace Gupo\ApidocGen\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RouteExport implements FromArray, WithHeadings, WithStyles
{
    private array $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [[
            '1、URL必须以"/"开头' . PHP_EOL
            . '2、请求类别只支持["get"]或["post"]，同时支持get和post时：["get","post"]' . PHP_EOL
            . '3、排序值为int，数值高的会在列表中排在上方' . PHP_EOL
            . '4、是否鉴权：填是时，网关会先判断是否登录，再判断用户所属角色是否拥有此接口权限。填否时，此接口为公开接口' . PHP_EOL
            . '5、是否必选权限：填是时，默认为所有用户分配此权限。填否时，需要通过功能权限勾选与角色分配。' . PHP_EOL
            . '6、上级URL:用于构建接口的树状结构，方便管理与后期分配' . PHP_EOL],
            ['接口名称', '请求类别', 'URL', '上级URL', '是否鉴权', '是否必选权限', '排序值']
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A1:G1');
        // 设置高度
        $sheet->getRowDimension(1)->setRowHeight(100);
        // 设置换行
        $sheet->getStyle('A1:G1')->getAlignment()->setWrapText(true);
        // 设置顶端对齐
        $sheet->getStyle('A1:G1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        // 将需要填写的单元格设置为红色
        $sheet->getStyle('A1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $sheet->getStyle('A2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $sheet->getStyle('B2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $sheet->getStyle('C2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $sheet->getStyle('E2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $sheet->getStyle('F2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        // 设置列宽
        $sheet->getColumnDimension('A')->setWidth(50);
        $sheet->getColumnDimension('C')->setWidth(50);
        $sheet->getColumnDimension('D')->setWidth(50);
    }
}
